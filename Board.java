public class Board{
	Tile[][] grid;
	public Board(){
		this.grid=new Tile[3][3];
		for(int row=0; row<grid.length; row++){
			for(int col=0; col<grid[row].length; col++){
				grid[row][col]=Tile.BLANK;
			}
		}
		
	}
	
	//Overwrite toString
	public String toString(){
		String result="";
		for(int row=0; row<grid.length; row++){
			for(int col=0; col<grid[row].length; col++){
				result+=this.grid[row][col].getName()+" ";
			}
			result+="\n";
		}
		return result;
	}
	
	//custom method
	public boolean placeToken(int row, int col, Tile playerToken){
		//1.Data Validation
		if(!(row>=0&&row<this.grid.length&&col>=0&&col<this.grid[0].length)){
			System.out.println("Error: Invalid row, column or token");
			return false;
		}
		
		//2.Process
		this.grid[row][col]=playerToken;
		return true;
	}
	
	public boolean checkIfFull(){
		for(int row=0; row<this.grid.length; row++){
			for(int col=0; col<this.grid[row].length;col++){
				if(this.grid[row][col]==Tile.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Tile playerToken){
		int tokenCounter=0;
		for(int row=0; row<this.grid.length; row++){
			for(int col=0; col<this.grid[row].length;col++){
				if(playerToken.equals(this.grid[row][col])){
					tokenCounter++;
				}
				if(tokenCounter==this.grid.length){
					return true;
				}
			}
			tokenCounter=0;
		}
		return false;
	}
	
	private boolean checkIfWinningfVertical(Tile playerToken){
		int tokenCounter=0;
		for(int col=0; col<this.grid.length; col++){
			for(int row=0; row<this.grid.length;row++){
				if(playerToken.equals(this.grid[row][col])){
					tokenCounter++;
				}
				if(tokenCounter==this.grid.length){
					return true;
				}
			}
			tokenCounter=0;
		}
		return false;
	}
	
	public boolean checkIfWinning(Tile playerToken){
		if(checkIfWinningHorizontal(playerToken)||checkIfWinningfVertical(playerToken)){
			return true;
		}
		return false;
	}
	
	private boolean checkIfWinningDiagonal(Tile playerToken){
	
	}
	
}